variable "region" {
  type        = string
  description = "What region would you like to deploy to?"
}

variable "prefix" {
  type        = string
  description = "What value should be prefixed to the names of resources?"
}

variable "vpc_id" {
  type        = string
  description = "What is the id of the VPC you want to deploy to?"
}

variable "dns" {
  type        = bool
  description = "Would you like a secure domain name?"
}

variable "domain" {
  type        = string
  description = "What is the domain name / hosted zone name to use with the dns feature?"
}

variable "user" {
  type        = string
  description = "What user name would you like tagged on each resource?"
}

variable "orchard_app_data" {
  type        = string
  description = "What folder do you want Orchard Core to use for storage?"
}

variable "efs_id" {
  type        = string
  description = "What is the id of the EFS you want to use?"
}

variable "pipeline_id" {
  type        = string
  description = "The image tag"
}
